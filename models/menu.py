# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('ArtAscii'), False, URL('index'), []),
    (T('Novo'), False, URL('artAscii'),[])
]

DEVELOPMENT_MENU = True 


if "auth" in locals(): auth.wikimenu() 
