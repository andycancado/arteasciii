from gluon.storage import Storage
settings = Storage()

settings.prodution = False

if settings.prodution:
    settings.db_uri = 'sqlite://prodution.sqlite'
    settings.migrate = False
else:
    
    settings.db_uri = 'sqlite://development.sqlite'
    settings.migrate = True

settings.title = request.application

settings.subtitle = 'qualquer coise'
settings.author = 'andy'
settings.author_email = 'andycancado@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.security_key = '7aa0d68a87d693f7fde24b6749ddbc29'
settings.email_server = 'localhost'
settings.email_sender = 'andycancado@gmail.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
